import 'package:flutter/material.dart';

class Assignment2 extends StatefulWidget {
  const Assignment2({super.key});

  @override
  State<Assignment2> createState() => _Assignment2State();
}

class _Assignment2State extends State<Assignment2> {
  bool box1Color = false;
  bool box2Color = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Color Box"),
      ),
      backgroundColor: Colors.grey.shade400,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Column(
                children: [
                  Container(
                    height: 200,
                    width: 200,
                    color: box1Color ? Colors.red : Colors.black,
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        box1Color = !box1Color;
                      });
                    },
                    child: const Text("Change Color"),
                  ),
                ],
              ),
              const SizedBox(
                width: 100,
              ),

              //Box2
              Column(
                children: [
                  Container(
                    height: 200,
                    width: 200,
                    color: box2Color ? Colors.blue : Colors.black,
                  ),
                  const SizedBox(
                    height: 50,
                  ),
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        box2Color = !box2Color;
                      });
                    },
                    child: const Text("Change Color"),
                  ),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
